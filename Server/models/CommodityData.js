var request = require('request-promise');
var cheerio = require('cheerio');
var _ = require('lodash');
const DataTypes = require('sequelize');
var Promise = require('bluebird');
var sequelize = require('../utils/MysqlSequelize');
var config = require('../config');

const SOURCES = {
    "future": {
        "url": config.commodity.futuresUrl,
        "tables": Object.keys(config.commodity.future),
        "model": {
            name: {
                type: DataTypes.STRING,
                unique: 'composite_index',
                primaryKey: true
            },
            symbol: DataTypes.STRING,
            exchange: DataTypes.STRING,
            price: DataTypes.STRING,
            chg1d: DataTypes.STRING,
            chgMtd: DataTypes.STRING,
            openInterest: DataTypes.STRING,
            commodity: DataTypes.STRING
        }
    },
    "spot": {
        "url": config.commodity.spotsUrl,
        "tables": Object.keys(config.commodity.spot),
        "model": {
            name: {
                type: DataTypes.STRING,
                unique: 'composite_index',
                primaryKey: true
            },
            price: DataTypes.STRING,
            units: DataTypes.STRING,
            asOf: DataTypes.STRING,
            source: DataTypes.STRING,
            market: DataTypes.STRING,
            chg1d: DataTypes.STRING,
            chgMtd: DataTypes.STRING,
            chgYtd: DataTypes.STRING,
            alternativeSource: DataTypes.STRING,
            commodity: DataTypes.STRING
        },
    }
};

const HEADERS = {
    'User-Agent'      : 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu' +
    ' Chromium/51.0.2704.79 Chrome/51.0.2704.79 Safari/537.36',
};

const CommoditySpot = sequelize.define('CommoditySpot', SOURCES.spot.model);

const CommodityFuture = sequelize.define('CommodityFuture', SOURCES.future.model);

const AllSpotModel = _.extend(SOURCES.spot.model, {
    market: DataTypes.STRING,
});
const AllSpot = sequelize.define('AllSpot', AllSpotModel);

const AllFutureModel = _.extend(SOURCES.future.model, {
    market: DataTypes.STRING,
});
const AllFuture = sequelize.define('AllFuture', AllFutureModel);

/**
 * Parse the html body: extract data from the results table into a csv.
 * @param body  the html body
 * @returns Array the csv, with headers
 */
function bodyToCSV(body, verbose = false ){
    var $ = cheerio.load( body );

    $.prototype.parsetable = function(dupCols, dupRows, textMode) {
        if (dupCols === undefined) dupCols = false;
        if (dupRows === undefined) dupRows = false;
        if (textMode === undefined) textMode = false;

        var resultTable = [],
            curr_x = 0,
            curr_y = 0;
        
        var rows = $("tr", this).length;

        $("tr", this).each(function(row_idx, row) {
            curr_y = 0;
            $("td, th", row).each(function(col_idx, col) {
                var rowspan = $(col).attr('rowspan') || 1;
                var colspan = $(col).attr('colspan') || 1;
                if (textMode === true) {
                    var content = $(col).text().trim() || "";
                } else {
                    var content = $(col).html() || "";
                }

                var x = 0,
                    y = 0;
                for (x = 0; x < rowspan && (curr_x + x < rows); x++) {
                    for (y = 0; y < colspan; y++) {
                        if (resultTable[curr_x + x] === undefined) {
                            resultTable[curr_x + x] = []
                        }

                        while (resultTable[curr_x + x][curr_y + y] !== undefined) {
                            curr_y += 1
                            if (resultTable[curr_x + x] === undefined) {
                                resultTable[curr_x + x] = []
                            }
                        }

                        if ((x === 0 || dupRows) && (y === 0 || dupCols)) {
                            resultTable[curr_x + x][curr_y + y] = content
                        } else {
                            resultTable[curr_x + x][curr_y + y] = ""
                        }
                    }
                }
                curr_y += 1;
            });
            curr_x += 1;
        });

        var chartColIndex = null;
        // Remove chart column
        resultTable[0].forEach(function(colName, index){
            if( colName.toLowerCase().includes("chart") ){
                for(var r=1; r<resultTable.length; r++){
                    resultTable[r].splice(index, 1);
                }
                chartColIndex = index;
            }
        });
        
        if( chartColIndex !== null ){
            resultTable[0].splice(chartColIndex, 1);
        }

        

        return resultTable;
    };

    var results = {};

    var tables = $('table').toArray();

    tables.forEach(function(table){
        var title = $(table).prevAll('h2').first().text();

        if( _.isEmpty(title) )
            return;

        if( $(table).find('th').length === 0 )
            return;

        var data = $(table).parsetable(true, true, true);
        // results[title] = transpose(data);
        results[title] = data;
    });

    return results;
}

class CommodityData{

    static _doRequest(target, database, callback, category = null){
        request({
            uri: target.url,
            headers: HEADERS,
            transform: function(body){
                var $ = cheerio.load( body );
                return bodyToCSV(
                    $('#content > noscript').first().text(), 
                    false);
            }
        })
        .then(function(data){
            var objects = [];
            for(var title in data ){
                if( !_.isEmpty(category) ){
                    // deal with getSpotsDetail/getFuturesDetail
                    var rows = data[title].slice(1);
                    var keys = Object.keys(target.model);
                    rows.forEach(function(row){
                        var object = {
                            "market": title,
                            "commodity": category
                        };
                        for(var index in row){
                            if( index < keys.length &&
                                _.isEmpty(object[keys[index]]) )
                                object[keys[index]] = row[index];
                        }
                        objects.push(object);
                    });
                }else if( target.tables.indexOf(title) !== -1 ){
                    // deal with getSpotSummary/getFutureSummary
                    var rows = data[title].slice(1);
                    var keys = Object.keys(target.model);
                    rows.forEach(function(row){
                        var object = {
                            "commodity": title
                        };
                        for(var index in row){
                            if( index < keys.length && 
                                _.isEmpty(object[keys[index]]) )
                                object[keys[index]] = row[index];
                        }
                        objects.push(object);
                    });
                }
            }
            sequelize.sync()
                .then(function(){
                    database.bulkCreate(objects, {
                        updateOnDuplicate: true
                    });
                });
            return data;
        })
        .catch(function (err) {
            throw err;
        })
        .nodeify(callback);
    }

    static _getFetchUrl(category, target){
        category = category
                    .toLowerCase()
                    .replace("-", " ")
                    .replace("-", " ");
        for(var prop in target){
            if( typeof target[prop] === "string" ){
                if( prop.toLowerCase() === category ){
                    return config.commodity.baseUrl + target[prop];
                }
            }else{
                for(var _prop in target[prop]){
                    if( _prop.toLowerCase() === category ){
                        return config.commodity.baseUrl + target[prop][_prop];
                    }
                }
            }
        }
        return "";
    }

    static getFutureSummary(callback){
        CommodityData._doRequest(
            SOURCES.future, 
            CommodityFuture,
            callback
        );
    }

    static getSpotSummary(callback){
        CommodityData._doRequest(
            SOURCES.spot, 
            CommoditySpot,
            callback
        );
    }

    static getSpotsDetail(category, callback){
        CommodityData._doRequest({
                url: CommodityData._getFetchUrl(category, config.commodity.spot),
                model: AllSpotModel
            },
            AllSpot,
            callback,
            category
        );
    }

    static getFuturesDetail(category, callback){
        CommodityData._doRequest({
                url: CommodityData._getFetchUrl(category, config.commodity.future),
                model: AllFutureModel
            },
            AllFuture,
            callback,
            category);
    }

    static getCommodityQuotes(category, name, callback){
        request({
            uri: CommodityData._getFetchUrl(category, config.commodity.spot),
            headers: HEADERS,
            transform: function(body){
                var $ = cheerio.load( body );
                $ = cheerio.load($('#content > noscript').first().text());
                var quandlCode = $(`tr a:contains('${name}')`).attr('href');
                if( quandlCode.includes("www.quandl.com") ){
                    quandlCode = quandlCode
                        .replace("http://www.quandl.com/", "")
                        .replace("https://www.quandl.com/", "");
                }else if( quandlCode.startsWith("/") ){
                    quandlCode = quandlCode.substr(quandlCode.indexOf("/")+1);
                }
                return `https://www.quandl.com/api/v3/datasets/${quandlCode}.json?api_key=${config.commodity.apikey}`;
            }
        })
        .then(function(url){
            if( url ){
                request({
                    uri: url,
                    headers: HEADERS
                })
                    .then(function(data){
                        if( typeof data === "string" )
                            data = JSON.parse(data);
                        return data['dataset'];
                    })
                    .catch(function (err) {
                        callback(err);
                    })
                    .nodeify(callback);
            }else{
                throw new Error();
            }
        })
        .catch(function (err) {
            var err = new Error("Page Not Found");
            err.status = 404;
            callback(err);
        });
    }

    static getCurrencies(callback){
        CommodityData._doRequest({
                url: config.commodity.currenciesUrl,
                model: AllSpotModel
            },
            AllSpot,
            callback,
            "Currencies"
        );
    }

    static updateSummary(){
        let getSpot = Promise.promisify(CommodityData.getSpotSummary);
        let getFuture = Promise.promisify(CommodityData.getFutureSummary);
        return getSpot()
            .then(getFuture);
    }

    static updateDetail(){
        let getSpot = Promise.promisify(CommodityData.getSpotsDetail);
        let getFuture = Promise.promisify(CommodityData.getFuturesDetail);
        let getCommodity = Promise.promisify(CommodityData.getCommodityQuotes);
        let getCurrency = Promise.promisify(CommodityData.getCurrencies);

        return sequelize.sync()
            .then(function () {
                return Promise.resolve(config.commodity.spot)
                    .then(function (spotList) {
                        var targets = [];
                        for(let prop in spotList){
                            if(typeof spotList[prop] === "object"){
                                for(let _prop in spotList[prop]){
                                    if( !_.isEmpty(spotList[prop][_prop]) )
                                        targets.push(_prop);
                                }
                            }else{
                                if( !_.isEmpty(spotList[prop]) )
                                    targets.push(prop);
                            }
                        }
                        return targets;
                    })
                    .map(function (category) {
                        return getSpot(category)
                            .then(function (data) {
                                var targets = [];
                                Object.values(data).forEach(function (csvData) {
                                    if( csvData.length > 1 ){
                                        csvData.slice(1).forEach(function (row) {
                                            if( row.length > 1 )
                                                targets.push({ category: category, name: row[0] });
                                        });
                                    }
                                });
                                return targets;
                            })
                            .map(function (item) {
                                return getCommodity(item.category, item.name).delay(1000);
                            }, { concurrency: 1 })
                            .catch(function(err){
                                if( err.status != 404 &&  err.statusCode != 404 )
                                    console.error(err.stack);
                            });
                    })
                    .then(getCurrency)
                    .then(function () {
                        return Object.keys(config.commodity.future);
                    })
                    .map(function (category) {
                        return getFuture(category);
                    })
                    .catch(function(err){
                        if( err.status != 404 &&  err.statusCode != 404 )
                            console.error(err.stack);
                    });
            });
    }
}

module.exports = CommodityData;

// CommodityData.getSpotsDetail("Coal", function(err, result){
//     if( err ){
//         console.error(err.stack);
//     }else{
//         console.log(result);
//     }
// })

// CommodityData.getCommodityQuotes("coal", "Northern Appalachia Coal Price", function(err, result){
//     if( err ){
//         console.error(err.stack);
//     }else{
//         console.log(result);
//     }
// });

// CommodityData.getCurrencies(function(err, result){
//     if( err ){
//         console.error(err.stack);
//     }else{
//         console.log(result);
//     }
// })