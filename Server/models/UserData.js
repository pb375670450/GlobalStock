/**
 * Created by stardust on 2017/5/23.
 */

const Sequelize = require('sequelize');
const config = require('../config');
const sequelize = require('../utils/MysqlSequelize');
const DataTypes = require('sequelize');

function sequalizeTypeJson(name, defaultValue = null) {
    return {
        type: DataTypes.STRING,
        get: function () {
            return JSON.parse(this.getDataValue(name));
        },
        set: function (value) {
            this.setDataValue(name, JSON.stringify(value));
        },
        defaultValue: defaultValue
    };
}

const User = sequelize.define('User', {
    username: {
        type: Sequelize.STRING,
        unique: 'composite_index',
        primaryKey: true
    },
    password: Sequelize.STRING,
    isAdmin: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    },
    blockNews: Sequelize.STRING,
    showCommodities: sequalizeTypeJson('showCommodities',
        '["Commodity Indexes","Industrial Metals","Fruits and Nuts","Grains and Cereals","Farms and Fishery","Energy","Agriculture Softs","Forestry","Fertilizers and Chemicals","Rare Metals","Vegetable Oils"]'),
    showFutures: sequalizeTypeJson('showFutures',
        '["Government Bonds","Interest Rates","Global Equities","Base Metals","Currencies","Energy","Agriculture","Grains","Softs","Precious Metals","Other Indexes","US Equities"]'),
    showStocks: {
        type: Sequelize.STRING,
        defaultValue: "NASDAQ:AAPL\n" +
        "NYSE:GE\n" +
        "NASDAQ:MSFT\n" +
        "NASDAQ:GOOG\n" +
        "NYSE:BABA\n" +
        "NYSE:JPM"
    },
});

if( config.admin ){
    sequelize.sync()
        .then(function () {
            config.admin.forEach(function (item) {
                User.create({
                    username: item.username,
                    password: item.password,
                    isAdmin: true,
                }, {
                    ignoreDuplicates: true
                });
            });
        });
}

class UserData{

    static addUser(username, passwd, callback){
        sequelize.sync()
            .then(function () {
                return User.create({
                    username: username,
                    password: passwd,
                    isAdmin: false,
                }, {
                    ignoreDuplicates: true
                })
            })
            .then(function (data) {
                callback(null, data);
            })
            .catch(callback);
    }


    static removeUser(username, callback){
        sequelize.sync()
            .then(function () {
                return User.destroy({
                    where: {
                        username: username,
                    }
                })
            })
            .then(function (data) {
                callback(null, data);
            })
            .catch(callback);
    }

    static findUser(username, callback){
        sequelize.sync()
            .then(function () {
                return User.findOne({
                    where: {
                        username: username
                    }
                });
            })
            .then(function (data) {
                callback(null, data);
            })
            .catch(callback);
    }

    static listUser(callback){
        sequelize.sync()
            .then(function () {
                return User.findAll();
            })
            .then(function (data) {
                callback(null, data);
            })
            .catch(callback);
    }

    static updateUser(username, data, callback){
        sequelize.sync()
            .then(function () {
                return User.update(data, {
                    where: {
                        username: username
                    },
                    fields: ['blockNews', 'showCommodities', 'showFutures', 'showStocks']
                });
            })
            .then(function () {
                return User.findOne({
                    where: {
                        username: username
                    }
                });
            })
            .then(function (data) {
                callback(null, data);
            })
            .catch(callback);
    }

}

module.exports = UserData
