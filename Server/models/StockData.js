/**
 * Created by stardust on 2017/4/20.
 */
const Promise = require('bluebird');
const DataTypes = require('sequelize');
const _ = require('lodash');
const request = require('request-promise');
const fs = require('fs');
const path = require('path');
const os = require('os');
const sanitize = require("sanitize-filename");
const csvParse = Promise.promisify(require('csv').parse);


const sequelize = require('../utils/MysqlSequelize');
const googleFinance = require('../node-google-finance');
const config = require('../config');
const GoogleNewsRss = require('../google-news-rss');
// const redlock = require('../utils/redis-middleware').redlock;

const googleNews = new GoogleNewsRss();
const DEFAULT_PAGE = 0;
const DEFAULT_PAGE_SIZE = 10;

const MARKETNAME_MAPPINGS = {
    'AMEX': 'NYSEAMERICAN'
};

var tableSequelize = {};


config.stock.markets.forEach(function (market) {
    tableSequelize[market] = sequelize.define(market, {
        Symbol: {
            type: DataTypes.STRING,
            allowNull: true,
            primaryKey: true
        },
        Name: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        LastSale: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        MarketCap: {
            type: "DOUBLE",
            allowNull: true
        },
        ADR_TSO: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        IPOyear: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        Sector: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        Industry: {
            type: DataTypes.TEXT,
            allowNull: true
        },
        Summary_Quote: {
            type: DataTypes.TEXT,
            allowNull: true
        }
    }, {
        freezeTableName: true,
        // timestamps: false,
    });
});

function getCurrentTime() {
    let date = new Date();
    return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + date.toLocaleTimeString();
}

function getSymbol(market, code){
    if( MARKETNAME_MAPPINGS[market] )
        market = MARKETNAME_MAPPINGS[market];
    return market.trim() + ": " + code.trim();
}


function sequalizeTypeJson(name, defaultValue = null) {
    return {
        type: DataTypes.STRING,
        get: function () {
            return JSON.parse(this.getDataValue(name));
        },
        set: function (value) {
            this.setDataValue(name, JSON.stringify(value));
        },
        defaultValue: defaultValue
    };
}

let quotesSequelize = sequelize.define('stockQuote', {
    StockSymbol: {
        type: DataTypes.STRING,
        primaryKey: true,
        allowNull: false
    },
    Index: DataTypes.STRING,
    LastTradePrice: DataTypes.STRING,
    Change: DataTypes.STRING,
    ChangePercent: DataTypes.STRING,
    Open: DataTypes.STRING,
    High: DataTypes.STRING,
    Low: DataTypes.STRING,
    Volume: DataTypes.STRING,
    AverageVolume: DataTypes.STRING,
    High52Week: DataTypes.STRING,
    Low52Week: DataTypes.STRING,
    MarketCapitalization: DataTypes.STRING,
    PE: DataTypes.STRING,
    Beta: DataTypes.STRING,
    EPS: DataTypes.STRING,
    DividendYield: DataTypes.STRING,
    LatestDividend: DataTypes.STRING,
    Shares: DataTypes.STRING,
    InstitutionalOwnership: DataTypes.STRING,
    SectorName: DataTypes.STRING,
    IndustryName: DataTypes.STRING,
    Name: DataTypes.STRING,
    KeyRatioRecentQuarterDate: DataTypes.STRING,
    KeyRatioAnnualDate: DataTypes.STRING,
    Management: sequalizeTypeJson('Management'),
    KeyRatios: sequalizeTypeJson('KeyRatios'),
    Related: sequalizeTypeJson('Related'),
    Summary: sequalizeTypeJson('Summary')
});

let histSequelize = sequelize.define('stockHist', {
    symbol: DataTypes.STRING,
    date: DataTypes.STRING,
    open: DataTypes.STRING,
    high: DataTypes.STRING,
    low: DataTypes.STRING,
    close: DataTypes.STRING,
    volume: DataTypes.STRING,
});

class StockData{
    static getStockList(market, offset, limit, callback, sector = null, symbols = []){

        if( !config.stock.markets.includes(market) ){
            callback(new Error(`Market code error: ${market}`));
        }

        sequelize.sync()
            .then(function () {
                var where = {};
                if( sector )
                    where["Sector"] = sector;
                if( !_.isEmpty(symbols) ){
                    where[DataTypes.Op.or] = [];
                    symbols.forEach(function (item) {
                        where[DataTypes.Op.or].push({ "Symbol": item });
                    });
                }
                tableSequelize[market].findAll({
                    where: where,
                    // offset: offset,
                    // limit: limit,
                }).then(function (data) {
                    callback(null, data);
                }).catch(function (err){
                    callback(err);
                });
            })
            .catch(function (err) {
                callback(err);
            });

    }

    static getRealtimePrice(market, symbols, callback) {
        var list = [];
        symbols.forEach(function (v) {
            list.push(getSymbol(market, v));
        });
        googleFinance.quotes({
            symbols: list
        }, function (err, data) {
            if( !err ){
                sequelize.sync()
                    .then(function () {
                        var returnEmpty = false;
                        symbols.forEach(function (v) {
                            if( _.isEmpty(data[getSymbol(market, v)]) ){
                                returnEmpty = true;
                                return;
                            }
                        });
                        if( returnEmpty ){
                            return quotesSequelize.findAll({
                                where: { StockSymbol: symbols }
                            }).then(function (data) {
                                callback(null, data);
                            });
                        }else{
                            data = Object.values(data);
                            quotesSequelize.bulkCreate(data, {
                                updateOnDuplicate: true
                            });
                            callback(null, data);
                        }
                    })
                    .catch(function (err) {
                        callback(err);
                    });
            }else{
                callback(err);
            }
        });
    }

    static getStockHistData(market, code, from, to, callback) {
        googleFinance.historical({
            symbol: getSymbol(market, code),
            from: new Date(from || getCurrentTime()).toISOString(),
            to: new Date(to || new Date()).toISOString()
        }, function (err, data) {
            callback(err, data);
            if( !err ){
                sequelize.sync()
                    .then(function () {
                        histSequelize.bulkCreate(data, {
                            updateOnDuplicate: true
                        });
                    })
                    .catch(function (err) {
                        console.error(err.stack);
                    });
            }
        });
    }

    static getNews(name, lang, callback, limit, isSection = false){
        let success = function (data) {
            callback(null, data);
            if( !config.news.savePath )
                return;
            // const dirname = config.news.saveByCategory
            //     ? path.join(config.news.savePath, name)
            //     : config.news.savePath;
            const dirname = config.news.savePath;
            if( !fs.existsSync(dirname) ){
                fs.mkdirSync(dirname);
            }
            data.forEach(function (item) {
                request(item.link)
                    .then(function (html) {
                        let file = path.join(dirname, sanitize(item.title)+".html");
                        if( !fs.existsSync(file) ){
                            fs.writeFile(file, html, 'utf-8', function (err) {
                                if ( err ){
                                    console.error(err.stack);
                                }
                            });
                        }
                    })
                    .catch(function (err) {
                        console.error(err.stack);
                    });
            });
        };

        if( isSection ){
            googleNews
                .section(name,
                    limit ? parseInt(limit) : config.news.defaultLimit,
                    lang)
                .then(success)
                .catch(function(err){ callback(err, null); })
        }else{
            googleNews
                .search(name,
                    limit ? parseInt(limit) : config.news.defaultLimit,
                    lang)
                .then(success)
                .catch(function(err){ callback(err, null); })
        }
    }

    static updateStockLists(){
        if( !config.autoUpdateData )
            return;
        const urls = config.stock.listUrls;
        return sequelize.sync()
            .then(function () {
                return Promise
                    .resolve(Object.keys(urls))
                    .map(function (market) {
                        return request(urls[market])
                            .then(function (data) {
                                return csvParse(data);
                            })
                            .then(function (data) {
                                var columns = [], objects = [];
                                data[0].forEach(function(item){
                                    item = item.replace(" ", "_").trim();
                                    if( item.length > 0 )
                                        columns.push(item);
                                });

                                for(let i=1; i<data.length; i++){
                                    var object = {};
                                    columns.forEach(function(value, index){
                                        object[value] = data[i][index];
                                    });
                                    objects.push(object);
                                }
                                // tableSequelize[market].update()
                                return tableSequelize[market].bulkCreate(objects, {
                                    updateOnDuplicate: true
                                });
                            })
                    })
                    .then(function () {
                        console.log("Update Stock List Successful");
                        // callback(null);
                    });
            });
    }

    static updateStockHists(){
        let current = new Date();
        let dayInterval = 60*60*24*1000;
        if( !config.autoUpdateData )
            return;
        sequelize.sync()
            .then(function () {
                return Promise
                    .resolve(Object.keys(tableSequelize))
                    .map(function (market) {
                        return tableSequelize[market]
                        .findAll()
                        // .findAll({ limit: 10 })
                        .map(function (row) {
                            return histSequelize.max('date', {
                                    where: { symbol: getSymbol(market, row.Symbol) }
                                })
                                .then(function (latest) {
                                    if( _.isEmpty(latest) ){
                                        latest = new Date("2010-01-01");
                                    }else{
                                        latest = new Date(latest);
                                    }
                                    if( current - latest < dayInterval ){
                                        return null;
                                    }else{
                                        return googleFinance.historical({
                                            symbol: getSymbol(market, row.Symbol),
                                            from: latest.toISOString(),
                                            to: current.toISOString()
                                        })
                                        .then(function (data) {
                                            // console.log(`update ${data.length} stock hists for stock: ${market}:${row.Symbol}`);
                                            // updateQuotes
                                            if( data.length > 0 ){
                                                return histSequelize.bulkCreate(data, {
                                                    updateOnDuplicate: true
                                                });
                                            }else{
                                                return null;
                                            }
                                        });
                                    }
                                });
                        }, { concurrency: os.cpus().length })
                    })
                    .then(function () {
                        console.log("Update Stock Hist Data Successful");
                        // callback(null);
                    });
            })
    }

    static updateStockQuotes(){
        if( !config.autoUpdateData )
            return;
        sequelize.sync()
            .then(function () {
                return Promise
                    .resolve(Object.keys(tableSequelize))
                    .map(function (market) {
                        return tableSequelize[market]
                        .findAll()
                        // .findAll({ limit: 10 })
                        .map(function (row) {
                            return getSymbol(market, row.Symbol);
                        }, { concurrency: os.cpus().length })
                        .then(function (data) {
                            return googleFinance.quotes({
                                symbols: data
                            })
                            .then(function (data) {
                                console.log(`update ${Object.values(data).length} stock quotes for market: ${market}`);
                                return quotesSequelize.bulkCreate(Object.values(data), {
                                    updateOnDuplicate: true
                                });
                            });
                        });
                    });
            });
    }


    // static getComment(code, callback){
    //     var conn = getConnection();
    //     var sql = "SELECT * FROM `stockUserComments` WHERE `stockCode` = ?"
    //     conn.query(sql, [code], callback)
    // }
    //
    // /* SQL Injection
    // * */
    // static postComment(code, user, content, callback){
    //     var conn = getConnection();
    //     var sql = "INSERT IGNORE `stockUserComments`(`stockCode`, \
    //             `user`, \
    //         `date`, \
    //         `content`, \
    //         `approval`) VALUES(?, ?, ?, ?, 0)"
    //     conn.query(sql, [code, user, getCurrentTime(), content], callback)
    // }

}

// updateStockLists(function(err){
//     console.error(err.stack);
// });


module.exports = StockData;