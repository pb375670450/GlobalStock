var express = require('express');
var CommodityData = require('../models/CommodityData');
var config = require('../config');
var _ = require('lodash');
var redisGet = require('../utils/redis-middleware').redisGet;
var redisSet = require('../utils/redis-middleware').redisSet;
var transform = require('../utils/utils').transform;

var router = express.Router();

const EXPIRE_TIME = 60; // seconds


router.get('/:type', function (req, res, next) {
    var type = req.params.type;
    const COMMODITY_TYPES = {
        "future": CommodityData.getFutureSummary,
        "spot": CommodityData.getSpotSummary
    };

    var key = JSON.stringify({
        type: "commodityJson",
        commodityType: type,
        lang: req.session.lang
    });

    if( !Object.keys(COMMODITY_TYPES).includes(type) ){
        next();
    }else{
        redisGet(key, next, function (data) {
            res.json(data);
        }, function () {
            COMMODITY_TYPES[type](function (err, result) {
                if( err ){
                    next(err);
                }else{
                    redisSet(key, result, false, EXPIRE_TIME);
                    res.json(result);
                }
            });
        });
    }
});

router.get('/:type/:category', function (req, res, next) {
    var category = req.params.category;
    var type = req.params.type;
    const COMMODITY_TYPES = {
        "future": CommodityData.getFuturesDetail,
        "spot": CommodityData.getSpotsDetail
    };

    var key = JSON.stringify({
        type: "commodityByCategory",
        commodityType: type,
        category: category,
        lang: req.session.lang
    });

    if( !Object.keys(COMMODITY_TYPES).includes(type) ){
        next()
    }else{
        res.locals.type = type;
        res.locals.category = category;
        res.locals.urlTable = config.commodity[type];
        // console.log(res.locals.urlTable);

        redisGet(key, next, function (data) {
            res.render('commodity-bycategory', {
                data: data,
            }, transform(req, res, next));
        }, function () {
            COMMODITY_TYPES[type](category, function (err, data) {
                if( err ){
                    next(err);
                }else{
                    redisSet(key, data, false, EXPIRE_TIME);
                    res.render('commodity-bycategory', {
                        data: data,
                    }, transform(req, res, next));
                }
            });
        });
    }
});

router.get('/spot/:category/:name', function (req, res, next) {
    var category = req.params.category;
    var name = req.params.name;

    var key = JSON.stringify({
        type: "commodityQuote",
        name: name,
        category: category,
        lang: req.session.lang
    });

    res.locals.category = category;
    res.locals.shortName = name;

    redisGet(key, next, function (data) {
        // console.log(data);
        res.render('commodity-info', data);
    }, function () {
        CommodityData.getCommodityQuotes(category, name, function (err, data) {
            if( err ){
                next(err);
            }else{
                redisSet(key, data, false, EXPIRE_TIME);
                res.render('commodity-info', data);
            }
        });
    });
});

module.exports = router;

