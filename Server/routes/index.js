var express = require('express');
var StockData = require('../models/StockData');
var oneSignal = require('../utils/onesignal');
var UserData = require('../models/UserData');
var CommodityData = require('../models/CommodityData');
var config = require('../config');
var _ = require('lodash');
var redisGet = require('../utils/redis-middleware').redisGet;
var redisSet = require('../utils/redis-middleware').redisSet;
var transform = require('../utils/utils').transform;
var translate = require('../utils/utils').translate;

var router = express.Router();
var stockList = [];
var indexList = [];
let marketList = config.stock.markets;
const EXPIRE_TIME = 60;

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('home', {
        userdata: _.pick(res.locals.user, ['showCommodities', 'showFutures', 'showStocks']),
    }, transform(req, res, next));
});

router.get('/login', function (req, res, next) {
    res.render('login', {
        url: req.query.url
    }, transform(req, res, next));
});

router.post('/login', function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    var url = req.query.url;

    UserData.findUser(username, function (err, result) {
        if( err ){
            next(err);
        }else{
            // todo: encrypt password
            if( result && result.password === password ){
                req.session.user = result;
                res.json({
                    success: true,
                    url: url ? url : "/"
                });
            }else{
                res.json({
                    success: false
                });
            }
        }
    })
});

router.get('/logout', function (req, res, next) {
    req.session.user = null;
    res.redirect('/');
});

router.get('/manage', function (req, res, next) {
    if( res.locals.user ){
        UserData.listUser(function (err, result) {
            if( err ){
                next(err);
            }else{
                res.render('manage', {
                    users: res.locals.user.isAdmin ? result : []
                }, transform(req, res, next));
            }
        });
    }else{
        res.redirect('/login');
    }
});

router.get('/stock/:market/:stockid', function (req, res, next) {
    let stock = req.params.stockid;
    let market = req.params.market;
    // console.log(res.locals.session.user)
    if( !marketList.includes(market)){
        next();
    }else{
        StockData.getRealtimePrice(market, [stock], function (err, result) {
            if( err ) {
                next(err);
            }else{
                if( _.isEmpty(result) ){
                    var error = new Error("Page Not Found");
                    error.status = 404;
                    next(error);
                }else{
                    res.render('stockinfo', _.extend(Object.values(result)[0], {
                        // history: result,
                        host: req.get('Host'),
                        protocol: req.protocol,
                        isNumeric: function(n) {
                            return !isNaN(parseFloat(n)) && isFinite(n);
                        }
                    }), transform(req, res, next));
                }
            }
        });
    }
});

router.get('/markets', function (req, res, next) {
    res.render('markets', {
        markets: config.stock.markets,
        sectors: config.stock.sectors
    }, transform(req, res, next));
});


router.get('/:type', function (req, res, next) {
    const COMMODITY_TYPES = {
        "spot": CommodityData.getSpotSummary,
        "future": CommodityData.getFutureSummary,
    };
    if( COMMODITY_TYPES.hasOwnProperty(req.params.type) ){
        const type = req.params.type;
        let key = JSON.stringify({
            "type": `commodity-${type}`
        });

        res.locals.urlTable = {
            "spot": config.commodity.spot,
            "future": config.commodity.future
        };
        res.locals.type = type;

        redisGet(key, next, function (data) {
            res.render('commodity', data, transform(req, res, next));
        }, function () {
            CommodityData.getFutureSummary(function (err, futureData) {
                if( err ){
                    next(err);
                }else{
                    CommodityData.getSpotSummary(function (err, spotData) {
                        if( err ){
                            next(err);
                        }else{
                            var data = {
                                future: _.pick(futureData, Object.keys(config.commodity.future)),
                                spot: _.pick(spotData, Object.keys(config.commodity.spot)),
                            };
                            redisSet(key, JSON.stringify(data), false, EXPIRE_TIME);
                            res.render('commodity', data, transform(req, res, next));
                        }
                    });
                }
            });
        });
    }else{
        next();
    }

});

router.post('/translate', function (req, res, next) {
    var text = req.body.text;

    translate(text, req.session.lang, function (err, result) {
        if( err ){
            next(err);
        }else{
            res.send(result);
        }
    });
});


module.exports = router;
