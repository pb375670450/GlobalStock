/**
 * Created by stardust on 2017/4/20.
 */

var express = require('express');
var StockData = require('../models/StockData');
var ejs = require('ejs');
var fs = require('fs');
const sanitize = require("sanitize-filename");

var router = express.Router();
var redisSet = require('../utils/redis-middleware').redisSet;
var redisGet = require('../utils/redis-middleware').redisGet;
var transform = require('../utils/utils').transform;
var translate = require('../utils/utils').translate;

const commentTemplate = ejs.compile(fs.readFileSync('views/widgets/commentitem.ejs', 'utf-8'));
const newsTemplate = ejs.compile(fs.readFileSync('views/widgets/newsitem.ejs', 'utf-8'));
const EXPIRE_TIME = 60; // seconds

/* GET stock list of market */
router.get('/list/:market', function(req, res, next) {
    let key = JSON.stringify({
        type: "stockslist",
        market: req.params.market,
        limit: req.query.limit,
        offset: req.query.offset,
        sector: req.query.sector,
        symbols: req.query.symbols,
        lang: req.session.lang
    });
    let sector = req.query.sector;
    if( sector && sector.toUpperCase() === "ALL" )
        sector = null;

    redisGet(key, next, function(data){
        res.send(data);
    }, function () {
        StockData.getStockList(
            req.params.market,
            req.query.offset,
            req.query.limit,
            function (err, result) {
                if( err ){
                    next(err);
                }else{
                    redisSet(key, result, false, EXPIRE_TIME);
                    res.json(result);
                }
            }, sector, req.query.symbols);
    });
});

/* GET stock realtime quotes data. */
router.get('/realtime/:market', function(req, res, next) {
    let key = JSON.stringify({
        type: "realtime_quotes",
        market:req.params.market,
        symbols: req.query.symbols,
        lang: req.session.lang
    });
    redisGet(key, next,function(data){
        res.send(data);
    }, function () {
        StockData.getRealtimePrice(req.params.market, req.query.symbols, function (err, result) {
            if( err ){
                console.error(err.stack);
                res.json({message: "Invalid parameters"})
            }else{
                redisSet(key, result, false, EXPIRE_TIME);
                res.json(result);
            }
        });
    })
});

router.get('/hist/:market/:stockid', function (req, res, next) {
    let key = JSON.stringify({
        type: "hist",
        stock: req.params.stockid,
        market: req.params.market,
        from: req.query.from,
        to: req.query.to
    });

    redisGet(key, next, function (data) {
        res.send(data);
    }, function () {
        StockData.getStockHistData(
            req.params.market,
            req.params.stockid,
            req.query.from,
            req.query.to,
            function (err, result) {
                if( err ){
                    next(err);
                }else{
                    redisSet(key, result, false, EXPIRE_TIME);
                    res.json(result);
                }
            }
        );
    });
});


/* GET news of a term. */
router.get('/news/:name',function (req, res, next) {
    let blockNews = res.locals.user.blockNews ? res.locals.user.blockNews.split("\n") : [];
    let isSection = req.query.section;
    let key = JSON.stringify({
        type: "__news",
        name: req.params.name,
        limit: req.query.limit,
        block: blockNews,
        isSection: isSection,
        lang: req.session.lang
    });

    redisGet(key, next, function (data) {
        res.send(data);
    }, function () {
        StockData.getNews(req.params.name, req.session.lang, function (err, result) {
            if( err ){
                next(err);
            }else{
                var ret = "";
                result = result.sort(function (a, b) {
                    return Date.parse(b.pubDate) - Date.parse(a.pubDate);
                });
                result.forEach(function (news) {
                    if( !blockNews.includes(news.publisher) ){
                        data = Object.assign({}, news)
                        data.link = sanitize(news.title);
                        ret += newsTemplate(data);
                    }
                });
                if( isSection ){
                    redisSet(key, ret, false, EXPIRE_TIME);
                    res.send(ret);
                }else{
                    translate(ret, req.session.lang, function (err, result) {
                        if( err ){
                            next(err);
                        }else{
                            redisSet(key, result, false, EXPIRE_TIME);
                            res.send(result);
                        }
                    });
                }
            }
        }, req.query.limit, isSection);
    });

});

//
// router.get('/comment/:stockid', function (req, res) {
//     let key = JSON.stringify({
//         type: "realtime",
//         stock: req.params.stockid
//     });
//     StockData.getComment(req.params.stockid, function (err, result) {
//         if( err ){
//             console.log(err)
//             res.json({message: "Invalid parameters"})
//         }else{
//             var ret = ''
//             for(let index in result){
//                 ret += commentTemplate({ comment: result[index] }) + "\n"
//             }
//            res.send(ret)
//         }
//     })
// });
//
// /* upload new commens
// * {
// *     user: ,
//  *     text:
// * }
// * */
// router.post('/comment/:stockid', function (req, res) {
//     let key = JSON.stringify({
//         type: "realtime",
//         stock: req.params.stockid
//     })
//     if( req.body.user && req.body.text ){
//         StockData.postComment(req.params.stockid, req.body.user, req.body.text,
//             function (err, result) {
//                 if( err ){
//                     console.log(err)
//                     res.status(500).json()
//                 }else{
//                     res.status(201).json({ success: true })
//                 }
//             })
//     }else{
//         res.status(200).json({ message: "Invalid user or content"})
//     }
// });

module.exports = router;