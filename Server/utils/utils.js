const request = require('request-promise');
const config = require('../config');
const minify = require('html-minifier').minify;
const redisGet = require('./redis-middleware').redisGet;
const redisSet = require('./redis-middleware').redisSet;
const StockData = require('../models/StockData');
const CommodityData = require('../models/CommodityData');
const DataTypes = require('sequelize');

const EXPIRE_TIME = 600;        // seconds

class Utils{
    static translate(text, target, callback){
        if( target === config.languages[0] ){
            callback(null, text);
            return;
        }else{
            var body;
            if(typeof text === "string"){
                text = minify(text, {
                    removeAttributeQuotes: true,
                    collapseWhitespace: true,
                    minifyCSS: true,
                    minifyJS: true,
                    removeComments: true,
                    removeEmptyAttributes: true,
                });
                body = JSON.stringify({
                    target: target,
                    q: text
                });
            }else if( Array.isArray(text) ){
                body = '{';
                text.forEach(function (value) {
                    body += `'q': '${value}', `;
                });
                body += `'target':'${target}' }`;
            }else{
                callback(null, text);
                return;
            }

            let key = body;

            redisGet(key, function (err) {
                callback(err);
            }, function(data){
                callback(null, data);
            }, function () {
                request({
                    uri: "https://translation.googleapis.com/language/translate/v2?key=" + config.translate["google-api-key"],
                    method: "POST",
                    body: body,
                })
                    .then(function(data){
                        var res;
                        data = JSON.parse(data);
                        if( Array.isArray(text) ){
                            res = [];
                            data.data.translations.forEach(element => {
                                res.push(element.translatedText);
                            });
                        }else{
                            res = data.data.translations[0].translatedText;
                        }
                        redisSet(key, res);
                        return res;
                    })
                    .catch(function(err){
                        callback(err);
                    })
                    .nodeify(callback);
            });

        }

    }

    static transform(req, res, next, callback = null){
        return function (err, data) {
            if( err ){
                console.error(err.stack);
                next(err);
            }else{
                Utils.translate(data,
                    req.session.lang,
                    function (err, text) {
                        if( err ){
                            console.error(err.stack);
                            next(err);
                        }else if( callback ){
                            callback(text);
                        }else{
                            res.send(text);
                        }
                    })
            }
        }
    }

    static startUpdateRoutine() {
        if( config.autoUpdateData ){
            console.log("startUpdateRoutine started");
            // Stock
            StockData.updateStockLists()
                .then(StockData.updateStockQuotes);
            setInterval(function () {
                return StockData.updateStockLists()
                    .then(StockData.updateStockQuotes);
            }, config.stock.updateStockListInterval * 1000);

            StockData.updateStockHists();
            setInterval(StockData.updateStockHists, config.stock.updateStockHistInterval * 1000);

            // Commodity
            CommodityData.updateSummary();
            setInterval(CommodityData.updateSummary, config.commodity.updateSummaryInterval * 1000);

            CommodityData.updateDetail();
            setInterval(CommodityData.updateDetail, config.commodity.updateDetailInterval * 1000);
        }
    }
}

module.exports = Utils;