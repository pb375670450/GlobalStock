/**
 * Created by stardust on 2017/5/27.
 */

var redis = require('redis');
var config = require('../config');
// var Redlock = require('redlock');
let option = {
    port: config.redis.port,
    host: config.redis.host,
    expireTime: 60,
};

// exports.client = client;

exports.redisSet = function(key, value, notExpire = true, time) {
    let client = redis.createClient(option.port, option.host);
    if(typeof key !== "string"){
        key = JSON.stringify(key);
    }
    if( typeof value !== "string" )
        value = JSON.stringify(value);
    if( notExpire )
        return client.set(key, value);
    else
        return client.set(key, value, "EX", time ? time : option.expireTime)
};

exports.redisGet = function(key, next, success, fail) {
    let client = redis.createClient(option.port, option.host);

    if( config.redis.enable ){
        if(typeof key !== "string"){
            key = JSON.stringify(key);
        }
        client.get(key, function (err, reply) {
            if( err ){
                next(err);
            }else if( reply ){
                if(typeof reply === "string"){
                    try{
                        reply = JSON.parse(reply);
                    }catch (e){
                        // do nothing
                    }
                }
                success(reply);
            }else {
                fail();
            }
        });
    }else{
        fail();
    }

};

exports.middleware = (req, res, next) => {

    if( req.path === '/login' || req.session.user ){
        res.locals.user = null;
        res.locals.session = {};
        // console.log("session = " + JSON.stringify(req.session));
        if( req.session && req.session.user ){
            res.locals.user = req.session.user;
        }
        next();
    }else{
        res.redirect('/login?url=' + req.path);
    }

};
//
// exports.redlock = new Redlock([redis.createClient(option.port, option.host)], {
//     // the expected clock drift; for more details
//     // see http://redis.io/topics/distlock
//     driftFactor: 0.01, // time in ms
//
//     // the max number of times Redlock will attempt
//     // to lock a resource before erroring
//     retryCount:  100,
//
//     // the time in ms between attempts
//     retryDelay:  2000, // time in ms
//
//     // the max time in ms randomly added to retries
//     // to improve performance under high contention
//     // see https://www.awsarchitectureblog.com/2015/03/backoff.html
//     retryJitter:  200 // time in ms
// });
