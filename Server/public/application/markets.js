// set default market as NASDAQ
var market = "NASDAQ";
let selector = '#summary-table';

function translateText() {
    let translatePart = selector + " tbody";
    $.ajax({
        url: '/translate',
        type: 'post',
        contentType: "application/json",
        data: JSON.stringify({
            text: $(translatePart).html()
        }),
        success: function (translated) {
            $(translatePart).html(translated);
        }
    });
}

function refreshTable(url) {
    $(selector).bootstrapTable('refresh', {
        url: url
    });
}

$(document).ready(function () {

    $(selector).bootstrapTable({
        url: "/stocks/list/"+market,
        onPostBody: translateText,
        onSearch: translateText
    });

    $('.selectpicker').selectpicker({
        style: 'btn-default',
        size: 4
    });

    $('#select-market').selectpicker('val', market);

    $('#select-market').change(function (e) {
        market = $(this).val();
        refreshTable(`/stocks/list/${market}`);
    });

    $('#select-sector').change(function (e) {
        var market = $('#select-market').val();
        refreshTable(`/stocks/list/${market}?sector=${$(this).val()}`);
    });

});