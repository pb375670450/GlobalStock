$(document).ready(function () {

    if( colNames.includes("Volume") && colNames.includes("Open") ){
        drawKCurve();
    }else{
        drawCompareCurve();
    }

    buildNews();
});


function drawKCurve() {
    var ohlc = [],
        volume = [],
        dataLength = histData.length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]],

        i = 0;
    var iDate = colNames.indexOf('Date'),
        iOpen = colNames.indexOf('Open'),
        iClose = colNames.indexOf('Settle'),
        iHigh = colNames.indexOf('High'),
        iLow = colNames.indexOf('Low'),
        iVol = colNames.indexOf('Volume');

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            Date.parse(histData[i][iDate]), // the date
            histData[i][iOpen], // open
            histData[i][iHigh], // high
            histData[i][iLow], // low
            histData[i][iClose] // close
        ]);

        volume.push([
            Date.parse(histData[i][iDate]), // the date
            histData[i][iVol] // the volume
        ]);
    }
    // console.log(ohlc);
    // create the chart
    Highcharts.stockChart('commodity-curve', {

        rangeSelector: {
            selected: 1
        },

        title: {
            text: `${shortName} Historical`
        },

        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'OHLC'
            },
            height: '60%',
            lineWidth: 2
        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Volume'
            },
            top: '65%',
            height: '35%',
            offset: 0,
            lineWidth: 2
        }],

        tooltip: {
            split: true
        },

        series: [{
            type: 'candlestick',
            name: shortName,
            data: ohlc,
            dataGrouping: {
                units: groupingUnits
            }
        }, {
            type: 'column',
            name: 'Volume',
            data: volume,
            yAxis: 1,
            dataGrouping: {
                units: groupingUnits
            }
        }]
    });
}

function drawCompareCurve() {
    var seriesOptions = [];

    function createChart() {
        Highcharts.stockChart('commodity-curve', {

            rangeSelector: {
                selected: 4
            },

            yAxis: {
                labels: {
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },

            plotOptions: {
                series: {
                    compare: 'percent',
                    showInNavigator: true
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2,
                split: true
            },

            series: seriesOptions
        });
    }

    for(var i=1; i<colNames.length; i++){
        var data = [];
        histData.forEach(function (row) {
            data.push([Date.parse(row[0]), row[i]]);
        });
        seriesOptions.push({
            name: colNames[i],
            data: data
        });
    }

    createChart();

}

function buildNews() {
    $.get(`/stocks/news/${category} ${shortName}`, function (html, status) {
        if( isSuccess(status) ){
            $('#commodity-news-content-tab').html(html);
        }else {
            console.log('status = ' + status)
        }
    });

    $.get(`/stocks/news/${category} Commodity`, function (html, status) {
        if( isSuccess(status) ){
            $('#category-news-content-tab').html(html);
        }else {
            console.log('status = ' + status)
        }
    });
}
