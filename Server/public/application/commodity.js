
function isSuccess(status) {
    return status === "success"
}

function strongFormatter(val) {
    return "<strong>" + val + "</strong>"
}

function changeFormatter(value) {
    value = parseFloat(value);
    if( isNaN(value) )
        return "-";
    else{
        if( value < 0 )
            return "<span style='color:red'>" + value + "</span>"
        else
            return "<span style='color:green'>+" + value + "</span>"
    }
}


$(document).ready(function () {
    var newSearch = {
        "spot": "Commodity Market",
        "future": "Commodity Futures"
    };
    $.get("/stocks/news/" + newSearch[type], function (html, status) {
        if( isSuccess(status) ){
            $('#commodity-news').html(html);
        }else {
            console.log('status = ' + status)
        }
    });
});