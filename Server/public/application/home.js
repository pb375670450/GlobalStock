function isSuccess(status) {
    return status === "success"
}

function StockLinkFormatter(value, row) {
    // return value
    return `<a translate="no" href='/stock/${row.Market}/${row.StockSymbol}'>${value}</a>`;
}

function ChangeFormatter(value, row) {
    if( parseFloat(value) < 0 )
        return "<span style='color:red'>" + value
            + (row.ChangePercent ? ` (${row.ChangePercent}%)` : "")
            + "</span>";
    else
        return "<span style='color:green'>+" + value.replace("+", "")
            + (row.ChangePercent ? ` (+${row.ChangePercent}%)` : "")
            + "</span>";
}

function PriceFormatter(value) {
    return `<strong>${value}</strong>`;
}

function translateText(text, callback) {
    $.ajax({
        url: '/translate',
        type: 'post',
        contentType: "application/json",
        data: JSON.stringify({
            text: text
        }),
        success: function (translatedData) {
            callback(translatedData);
        }
    });
}

$(document).ready(function () {
    $.fn.bootstrapTable.defaults.classes = "table table-hover";
    var name = 'active';
    $('#nav-home').removeClass(name).addClass(name);
    $('#nav-index').removeClass(name);
    $('#nav-stock').removeClass(name);

    getStocks();
    getCommodities();
    getNews();
});

function getStocks() {
    var targets = {};
    var stocksTable = [];
    var selector = '#stocks-table';

    userdata.stocks.forEach(function (item) {
        var tmp = item.split(":");
        if ( !targets[tmp[0]] )
            targets[tmp[0]] = [];
        targets[tmp[0]].push(tmp[1]);
    });

    let markets = Object.keys(targets);

    for(let market in targets){
        let columns = [
            {
                field: 'StockSymbol',
                title: 'Symbol',
                formatter: StockLinkFormatter
            },
            {
                field: 'Name',
                title: 'Name',
                formatter: StockLinkFormatter
            },
            {
                field: 'LastTradePrice',
                title: 'Price',
                formatter: PriceFormatter
            },
            {
                field: 'Change',
                title: 'Change',
                formatter: ChangeFormatter
            },
            {
                field: 'Low',
                title: 'Low',
            },
            {
                field: 'High',
                title: 'High',
            },
            {
                field: 'Index',
                title: "Market",
            },
            {
                field: 'SectorName',
                title: 'Sector',
            },
            // {
                // field:
            // }
        ];
        $.get("/stocks/realtime/"+market+"?"+$.param({
            "symbols": targets[market]
        }), function (data, status) {
            if( isSuccess(status) ){
                data.forEach(function (item) {
                    if( targets[market].includes(item.StockSymbol) ){
                        item['Market'] = market;
                        stocksTable.push(item);
                    }
                });
            }
            if( stocksTable.length === userdata.stocks.length ){
                $(selector).bootstrapTable({
                    columns: columns,
                    data: stocksTable,
                    onPostBody: function () {
                        translateText($(selector).html(), function (html) {
                            $(selector).html(html);
                            $(selector + ' thead tr').addClass("info");
                            $(selector + ' td:first-child').addClass("success");
                        });
                    }
                });
            }
        });
    }

}


function getCommodities() {
    let table = {
        "spot": {
            "url": "/commodities/spot",
            "excludeTables": ["List of Data Sources"],
            "targets": userdata.commodities,
            "columns": [
                {
                    field: 'Name',
                    title: 'Name',
                },
                {
                    field: 'Price',
                    title: 'Price',
                    formatter: PriceFormatter
                },
                {
                    field: 'Units',
                    title: 'Units'
                },
                {
                    field: "Market",
                    title: "Market"
                },
                {
                    field: '1D Chg',
                    title: '1-Day Change',
                    formatter: ChangeFormatter
                },
                {
                    field: 'MTD Chg',
                    title: 'MTD Change',
                    formatter: ChangeFormatter
                },
                {
                    field: 'As Of',
                    title: 'Date'
                },
            ]
        },
        "futures": {
            "url": "/commodities/future",
            "excludeTables": ["Delisted Contracts"],
            "targets": userdata.futures,
            "columns": [
                {
                    field: "Exchange",
                    title: "Market"
                },
                {
                    field: 'Name',
                    title: 'Name',
                },
                {
                    field: 'Price',
                    title: 'Price',
                    formatter: PriceFormatter
                },
                {
                    field: '1-Day Chg',
                    title: '1-Day Change',
                    formatter: ChangeFormatter
                },
                {
                    field: 'MTD Chg',
                    title: 'MTD Change',
                    formatter: ChangeFormatter
                },
            ]
        }
    };

    for(let type in table){
        $.get({
            url: table[type].url,
            async: true,
            success: function (data) {
                var tableData = {};
                for(let category in data){
                    if( table[type].excludeTables.includes(category) )
                        continue;
                    if( !table[type].targets.includes(category) )
                        continue;
                    let headers = data[category][0];
                    let iName = headers.indexOf("Name");
                    data[category].slice(1).forEach(function (item) {
                        // if( table[type].targets.includes(item[iName]) ){
                        var object = {};
                        headers.forEach(function (value, index) {
                            object[value] = item[index];
                        });
                        if( !tableData.hasOwnProperty(category) ){
                            tableData[category] = [];
                        }
                        tableData[category].push(object);
                        // }
                    });
                }
                // console.log(tableData);
                for(let category in tableData){
                    let selector = `${type}-${category}`.split(" ").join("-");
                    let boxSelector = selector + "-box";
                    $(`#${type}-container`).append(
                        `<div class="box box-default" id="${boxSelector}">\n` +
                        '    <div class="box-header with-border bg-gray-light">\n' +
                        `        <h3 class="box-title">${category + (type === "futures" ? " Futures":"")}</h3>\n` +
                        '<div class="box-tools pull-right">\n' +
                        '    <button type="button" class="btn btn-box-tool" data-widget="collapse">\n' +
                        '        <i class="fa fa-minus"></i>\n' +
                        '    </button>\n' +
                        '</div>'+
                        '    </div>\n' +
                        '    <div class="box-body">\n' +
                        `        <table id="${selector}">\n` +
                        '        </table>\n' +
                        '    </div>\n' +
                        '</div>');
                    $('#'+selector).bootstrapTable({
                        columns: table[type].columns,
                        data: tableData[category],
                        onPostBody: function () {
                            translateText($('#'+boxSelector).html(), function (html) {
                                $('#'+boxSelector).html(html);
                                $('#'+selector + ' thead tr').addClass("info");
                                $('#'+selector + ' td:first-child').addClass("success");
                            });
                        }
                    });
                }
            }

        });
    }
}

function getNews() {
    $.get(`/stocks/news/Business?section=true`, function (html, status) {
        if( isSuccess(status) ){
            $('#news-list').html(html);
        }else {
            console.log('status = ' + status)
        }
    });
}