var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var randomstring = require('randomstring');
var cors = require('cors');
var session = require('express-session');
var _ = require('lodash');

var redis = require('./utils/redis-middleware');
var index = require('./routes/index');
var users = require('./routes/users');
var stocks = require('./routes/stocks');
var commodities = require('./routes/commodities');
var config = require('./config');
var StockData = require('./models/StockData');
var transform = require('./utils/utils').transform;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    // store: new redisStore({
    //     client: redis.client
    // }),
    secret: randomstring.generate({
        length: 128,
        charset: 'alphabetic'
    }),
    resave: true,
    saveUninitialized: true
}));

app.use(function (req, res, next) {
    res.locals.languages = config.languages;
    if( !req.session.lang ){
        req.session.lang = config.languages[0];     // set default lang
    }
    res.locals.lang = req.session.lang;
    res.locals.site = config.site;
    next();
});

app.use(redis.middleware);

app.use('/', index);
app.use('/users', users);
app.use('/stocks', stocks);
app.use('/commodities', commodities);

// catch 404 and forward to error handler
app.use(function(err, req, res, next) {
    var statusCode = err.status || err.statusCode;
    if( err === null || statusCode == 404 ){
        err = new Error('Page Not Found');
        err.status = 404;
    }
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    console.error(err.stack);
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    let status = err.status || err.statusCode || 500;
    res.status(status);
    res.render('error', { status:status, message: err.message});
});


var port = normalizePort(config.port || process.env.PORT || '3000');
app.set('port', port);


module.exports = app;

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

