var config = {
    // site's name display on layout
    "site": "GlobalStock",          
    // server listen port
    "port": 3000,            
    // auto update database or not       
    "autoUpdateData": false,        
    // MySQL server connection
    "mysql": {
        "host": "112.74.172.59",   
        "user": "root",
        "password": "123456",
        "port": 3306,
        // The database name for this project, 
        //  it must be created BEFORE start server
        "database": "globalstockdb"     
    },   
    // redis server connection
    "redis": {
        // redis server hostname
        "host": "112.74.172.59",    
        "port": 6379,
        // use redis for cache or not
        "enable": true              
    },
    "translate": {
        // see https://cloud.google.com/docs/authentication/api-keys
        "google-api-key": "AIzaSyCVHgDen19S4d8TWCGURvVPoaQwMjMUPkQ"     
    },
    // admin account lists
    "admin": [                      
        {
            "username": "admin",
            "password": "admin"
        }
    ],
    // supported languages
    "languages": [                  
        "en",   // default language, DON'T modify
        "zh-cn",
        "ar"
    ],
    "news": {
        // default news limit
        "defaultLimit": 10,         
        // default news language
        "language": 'en',           
        // save directory for news
        "savePath": "public/news/", 
    },
    "stock": {
        // interval(seconds) between update stock list of each market
        "updateStockListInterval": 3600,        
        // interval(seconds) between update stock historical data
        "updateStockHistInterval": 60*60*24,       
        // DON'T modify below
        "listUrls": {
            "NASDAQ": "http://www.nasdaq.com/screening/companies-by-industry.aspx?exchange=NASDAQ&render=download",
            "NYSE": "http://www.nasdaq.com/screening/companies-by-industry.aspx?exchange=NYSE&render=download",
            "AMEX": "http://www.nasdaq.com/screening/companies-by-industry.aspx?exchange=AMEX&render=download",
        },
        "markets": ["NASDAQ", "NYSE", "AMEX"],
        "sectors": [
            "All",
            "Finance",
            "Consumer Services",
            "Technology",
            "Public Utilities",
            "Capital Goods",
            "Basic Industries",
            "Health Care",
            "Energy",
            "Miscellaneous",
            "Transportation",
            "Consumer Non-Durables",
            "Consumer Durables",
        ],
    },
    "commodity": {
        // for quandl.com, see https://docs.quandl.com/docs#section-authentication
        "apikey": "yj2za-zELsTg3eNbJRxZ",       
        // interval(seconds) between update commodities/futures summaries
        "updateSummaryInterval": 60*60,  
        // interval(seconds) between update commodities detail infos
        "updateDetailInterval": 60*60*24,   
        // DON'T modify below
        "baseUrl": "https://www.quandl.com/collections",
        "spotsUrl": "https://www.quandl.com/collections/markets/commodities",
        "futuresUrl": "https://www.quandl.com/collections/futures",
        "currenciesUrl": "https://www.quandl.com/collections/markets/g-10-cross-rates",
        "future": {
            "Energy": "/futures/energy",
            "Grains": "/futures/grains",
            "Softs": "/futures/softs",
            "Agriculture": "/futures/agriculture",
            "Precious Metals": "/futures/metals",
            "Base Metals": "/futures/metals",
            "Currencies": "/futures/currencies",
            "US Equities": "/futures/equities",
            "Global Equities": "/futures/equities",
            "Other Indexes": "/futures/indices",
            "Government Bonds": "/futures/bonds",
            "Interest Rates": "/futures/rates"
        },
        "spot": {
            "Energy": {
                "Coal": "/markets/coal",
                "Natural Gas": "/markets/natural-gas",
                "Crude Oil": "/markets/crude-oil",
                "Gasoline": "/markets/gasoline"
            },
            "Industrial Metals": "/markets/industrial-metals",
            "Rare Metals": "/markets/rare-metals",
            "Grains and Cereals": "/markets/grains-and-cereals",
            "Agriculture Softs": null,
            "Fruits and Nuts": null,
            "Vegetable Oils": null,
            "Forestry": null,
            "Farms and Fishery": "/markets/farms-and-fishery",
            "Fertilizers and Chemicals": null,
            "Commodity Indexes": null
        },
        "dataSources": {
            "CME"	: "Chicago Mercantile Exchange",
            "ICE"	: "IntercontinentalExchange",
            "ODA"	: "Open Data for Africa",
            "JM"	: "Johnson Matthey",
            "LBMA"	: "London Bullion Markets Association",
            "LME"	: "London Metals Exchange",
            "OPEC"	: "Organization of the Petroleum Exporting Countries",
            "WB"	: "World Bank",
            "COM"	: "Wiki Commodities Database Market Data Center"
        }
    }
};

module.exports = config;
