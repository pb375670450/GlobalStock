# Global Stock

GlobalStock is a global financial portal that provide news, streaming quotes and charts about the global markets.
The information provided cover a broad variety of financial vehicles including Stocks, Commodities, Currencies, Futures.

### Data Source

- Markets with Stocks: [www.nasdaq.com](www.nasdaq.com)
- Stocks Quotes: [Google Finance](https://finance.google.com/finance) 
- News: [Google News](https://news.google.com)
- Commodities and Futures: [Quandl](https://www.quandl.com/) **(Need API-KEY)**
- Auto Translation: [Google Cloud Translation API](https://cloud.google.com/translate/docs/) **(Need API-KEY)**

### Dependencies

- Node.js: **v8.x**
- MySQL Server: v5.5.57
- redis-server: v2.8.4

## Directory Structure

```
GlobalStock
├── README.md
└── Server
    ├── app.js                  # Express application file
    ├── bin
    ├── config.js               # Configuration file
    ├── credentials.js
    ├── google-news-rss         # Google News module
    ├── models                  # DAO classes 
    ├── node-google-finance     # Google Finance module
    ├── package.json
    ├── public                  # static resouces
    ├── routes                  # Express routes
    ├── utils                   # utility functions and classes
    └── views                   # page templates(ejs)
```

## Configuration

/Server/config.js:
```javascript
var config = {
  // site's name display on layout
  "site": "GlobalStock",          
  // server listen port
  "port": 80,            
  // auto update database or not       
  "autoUpdateData": false,        
  // MySQL server connection
  "mysql": {
    "host": "localhost",   
    "user": "root",
    "password": "",
    "port": 3306,
    // The database name for this project, 
    //  it must be created BEFORE start server
    "database": "globalstockdb"     
  },   
  // redis server connection
  "redis": {
    // redis server hostname
    "host": "localhost",    
    "port": 6379,
    // use redis for cache or not
    "enable": true              
  },
  "translate": {
    // see https://cloud.google.com/docs/authentication/api-keys
    "google-api-key": ""     
  },
  // admin account lists
  "admin": [
    {
        "username": "admin",
        "password": "admin"
    }
  ],
  // supported languages
  "languages": [                  
    "en",   // default language, DON'T modify
    "zh-cn",
    "ar"
  ],
  "news": {
    // default news limit
    "defaultLimit": 10,         
    // default news language
    "language": 'en',           
    // save directory for news 
    "savePath": "public/news/", 
    // if false, you could access the news via url: /hostname:port/news/[news title]
    "saveByCategory": false     
  },
  "stock": {
    // interval(seconds) between update stock list of each market
    "updateStockListInterval": 3600,        
    // interval(seconds) between update stock historical data
    "updateStockHistInterval": 60*60*24,       
    // ...
  },
  "commodity": {
    // for quandl.com, see https://docs.quandl.com/docs#section-authentication
    "apikey": "",       
    // interval(seconds) between update commodities/futures summaries
    "updateSummaryInterval": 60*60,  
    // interval(seconds) between update commodities detail infos
    "updateDetailInterval": 60*60*24,   
    // ...
  }
};

```

## Deployment

Please ensure you've set up the configuration in /Server/config.js, especially the database settings in `"mysql"`, `"translate"."google-api-key"` and `"commodity"."apikey"`.

And then started up MySQL/redis server **before** running the following commands.

## Example: Ubuntu 16.04 LTS

```bash
# install nodejs
$ sudo apt-get update
$ curl -sL https://deb.nodesource.com/setup_8.x -o nodesource_setup.sh
$ sudo bash nodesource_setup.sh
$ sudo apt-get install nodejs build-essential
# install and start redis server
$ sudo apt-get install redis-server
$ sudo service redis-server start

# decompress the project
$ unzip GlobalStock.zip
# Change current directory to the project root
$ cd GlobalStock
# Get dependencies package from remote repositories
$ git submodule update --init --recursive
# install modules for dependencies
$ cd Server/node-google-finance && npm install
$ cd ../google-news-rss && npm install
# install modules for this project
$ cd .. && npm install
# start server after **setting up config.js**
$ npm start
```